package com.gitlab.cdagaming.exodusclient.config;

import com.gitlab.cdagaming.exodusclient.Constants;
import com.gitlab.cdagaming.exodusclient.handler.StringHandler;
import com.google.common.collect.Lists;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

public class ConfigHandler {
    // Config Names
    // GENERAL
    public String NAME_fullBright, NAME_enableCommands;

    // Config Variables
    // GENERAL
    public boolean enableCommands, fullBright;

    // CLASS-SPECIFIC - PUBLIC
    public boolean hasChanged = false;
    // CLASS-SPECIFIC - PUBLIC
    public Properties properties = new Properties();
    // CLASS-SPECIFIC - PRIVATE
    private String fileName;
    private boolean verified = false, initialized = false, isConfigNew = false;

    public ConfigHandler(String fileName) {
        this.fileName = fileName;
    }

    public void setupInitialValues() {
        // GENERAL
        NAME_enableCommands = Constants.TRANSLATOR.translate("gui.config.name.general.enablecommands").replaceAll(" ", "_");
        NAME_fullBright = Constants.TRANSLATOR.translate("gui.config.name.general.fullbright").replaceAll(" ", "_");

        enableCommands = true;
        fullBright = false;

        initialized = true;
    }

    public void initialize() {
        try {
            File configFile = new File(fileName);
            File parentDir = configFile.getParentFile();
            isConfigNew = (!parentDir.exists() && parentDir.mkdirs()) || (!configFile.exists() && configFile.createNewFile());
            setupInitialValues();
        } catch (Exception ex) {
            Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.config.save"));
            ex.printStackTrace();
        } finally {
            if (initialized) {
                if (isConfigNew) {
                    updateConfig();
                }
                read();
            }
        }
    }

    public void read() {
        try {
            Reader configReader = new InputStreamReader(new FileInputStream(fileName), Charset.forName("UTF-8"));
            properties.load(configReader);
            configReader.close();
        } catch (Exception ex) {
            Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.config.save"));
            ex.printStackTrace();
        } finally {
            try {
                // GENERAL
                enableCommands = StringHandler.isValidBoolean(properties.getProperty(NAME_enableCommands)) ? Boolean.parseBoolean(properties.getProperty(NAME_enableCommands)) : enableCommands;
                fullBright = StringHandler.isValidBoolean(properties.getProperty(NAME_fullBright)) ? Boolean.parseBoolean(properties.getProperty(NAME_fullBright)) : fullBright;
            } catch (NullPointerException ex) {
                verifyConfig();
            } finally {
                if (!verified) {
                    verifyConfig();
                }
                Constants.LOG.info(Constants.TRANSLATOR.translate("exodusclient.logger.info.config.save"));
            }
        }
    }

    public void updateConfig() {
        // GENERAL
        properties.setProperty(NAME_enableCommands, Boolean.toString(enableCommands));
        properties.setProperty(NAME_fullBright, Boolean.toString(fullBright));

        // Check for Conflicts before Saving (NONE ATM)

        save();
    }

    private void verifyConfig() {
        List<String> validProperties = Lists.newArrayList();
        List<String> removedProperties = Lists.newArrayList();
        boolean needsFullUpdate = false;

        for (Field field : getClass().getDeclaredFields()) {
            if (field.getName().contains("NAME_")) {
                try {
                    field.setAccessible(true);
                    Object value = field.get(this);
                    validProperties.add(value.toString());
                    if (!properties.stringPropertyNames().contains(value.toString()) && validProperties.contains(value.toString())) {
                        Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.config.emptyprop", value.toString()));
                        needsFullUpdate = true;
                    }
                } catch (Exception ignored) {
                }
            }
        }

        for (String property : properties.stringPropertyNames()) {
            if (!validProperties.contains(property)) {
                Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.config.invalidprop", property));
                removedProperties.add(property);
                properties.remove(property);
                save();
            }
            if (!removedProperties.contains(property)) {
                if (StringHandler.isNullOrEmpty(properties.getProperty(property))) {
                    Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.config.emptyprop", property));
                    needsFullUpdate = true;
                } else {
                    // N/A
                }
            }
        }

        if (needsFullUpdate) {
            setupInitialValues();
            verified = true;
            if (!properties.stringPropertyNames().isEmpty()) {
                read();
            }
            updateConfig();
        } else {
            verified = true;
        }
    }

    public void save() {
        try {
            Writer configWriter = new OutputStreamWriter(new FileOutputStream(new File(fileName)), Charset.forName("UTF-8"));
            properties.store(configWriter, null);
            configWriter.close();
        } catch (Exception ex) {
            Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.config.save"));
            ex.printStackTrace();
        }
    }
}