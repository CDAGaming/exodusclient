package com.gitlab.cdagaming.exodusclient.config.gui;

import com.gitlab.cdagaming.exodusclient.Constants;
import com.gitlab.cdagaming.exodusclient.ExodusClient;
import com.gitlab.cdagaming.exodusclient.handler.CommandHandler;
import com.gitlab.cdagaming.exodusclient.handler.StringHandler;
import com.gitlab.cdagaming.exodusclient.handler.commands.CommandsGUI;
import com.gitlab.cdagaming.exodusclient.handler.gui.controls.GUIExtendedButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Keyboard;

import java.io.IOException;

public class ConfigGUI_Main extends GuiScreen {
    private final GuiScreen parentScreen, currentScreen;
    private GUIExtendedButton generalSet, proceedButton, aboutButton, commandGUIButton;

    public ConfigGUI_Main(GuiScreen parentScreen) {
        mc = ExodusClient.instance;
        currentScreen = this;
        this.parentScreen = parentScreen;
    }

    @Override
    public void initGui() {
        ExodusClient.GUIS.configGUIOpened = true;
        Keyboard.enableRepeatEvents(true);
        ScaledResolution sr = new ScaledResolution(mc);

        int calc1 = (sr.getScaledWidth() / 2) - 183;
        int calc2 = (sr.getScaledWidth() / 2) + 3;

        generalSet = new GUIExtendedButton(100, calc1, ExodusClient.GUIS.getButtonY(1), 180, 20, Constants.TRANSLATOR.translate("gui.config.title.general"));

        proceedButton = new GUIExtendedButton(700, (sr.getScaledWidth() / 2) - 90, (sr.getScaledHeight() - 30), 180, 20, Constants.TRANSLATOR.translate("gui.config.buttonMessage.back"));
        aboutButton = new GUIExtendedButton(800, 10, (sr.getScaledHeight() - 30), 20, 20, "?");
        commandGUIButton = new GUIExtendedButton(900, (sr.getScaledWidth() - 120), (sr.getScaledHeight() - 30), 85, 20, Constants.TRANSLATOR.translate("gui.config.title.commands"));

        buttonList.add(generalSet);

        buttonList.add(proceedButton);
        buttonList.add(aboutButton);
        buttonList.add(commandGUIButton);

        super.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        ScaledResolution sr = new ScaledResolution(mc);
        drawDefaultBackground();

        final String title = Constants.TRANSLATOR.translate("gui.config.title");

        drawString(fontRenderer, title, (sr.getScaledWidth() / 2) - (fontRenderer.getStringWidth(title) / 2), 20, 0xFFFFFF);

        commandGUIButton.enabled = ExodusClient.CONFIG.enableCommands;

        proceedButton.displayString = ExodusClient.CONFIG.hasChanged ? Constants.TRANSLATOR.translate("gui.config.buttonMessage.save") : Constants.TRANSLATOR.translate("gui.config.buttonMessage.back");

        super.drawScreen(mouseX, mouseY, partialTicks);

        if (ExodusClient.GUIS.isMouseOver(mouseX, mouseY, generalSet)) {
            ExodusClient.GUIS.drawMultiLineString(StringHandler.splitTextByNewLine(Constants.TRANSLATOR.translate("gui.config.comment.title.general")), mouseX, mouseY, width, height, -1, fontRenderer, true);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == generalSet.id) {
            mc.displayGuiScreen(new ConfigGUI_GeneralSettings(currentScreen));
        } else if (button.id == proceedButton.id) {
            if (ExodusClient.CONFIG.hasChanged) {
                ExodusClient.CONFIG.updateConfig();
                ExodusClient.CONFIG.read();
                CommandHandler.reloadData();
                ExodusClient.CONFIG.hasChanged = false;
            }

            if (mc.player != null) {
                ExodusClient.GUIS.configGUIOpened = false;
                mc.player.closeScreen();
            } else {
                ExodusClient.GUIS.configGUIOpened = false;
                mc.displayGuiScreen(parentScreen);
            }
        } else if (button.id == aboutButton.id) {
            mc.displayGuiScreen(new ConfigGUI_About(currentScreen));
        } else if (button.id == commandGUIButton.id) {
            mc.displayGuiScreen(new CommandsGUI(currentScreen));
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            if (ExodusClient.CONFIG.hasChanged) {
                ExodusClient.CONFIG.setupInitialValues();
                ExodusClient.CONFIG.read();
                ExodusClient.CONFIG.hasChanged = false;
            }
            ExodusClient.GUIS.configGUIOpened = false;
        }
        super.keyTyped(typedChar, keyCode);
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }
}
