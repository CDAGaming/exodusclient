package com.gitlab.cdagaming.exodusclient.config.gui;

import com.gitlab.cdagaming.exodusclient.Constants;
import com.gitlab.cdagaming.exodusclient.ExodusClient;
import com.gitlab.cdagaming.exodusclient.handler.StringHandler;
import com.gitlab.cdagaming.exodusclient.handler.gui.controls.GUICheckBox;
import com.gitlab.cdagaming.exodusclient.handler.gui.controls.GUIExtendedButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Keyboard;

import java.io.IOException;

public class ConfigGUI_GeneralSettings extends GuiScreen {
    private final GuiScreen parentScreen, currentScreen;
    private GUIExtendedButton proceedButton;
    private GUICheckBox enableCommandsButton, enableFullBrightButton;

    ConfigGUI_GeneralSettings(GuiScreen parentScreen) {
        mc = ExodusClient.instance;
        currentScreen = this;
        this.parentScreen = parentScreen;
    }

    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        ScaledResolution sr = new ScaledResolution(mc);

        int calc1 = (sr.getScaledWidth() / 2) - 145;
        int calc2 = (sr.getScaledWidth() / 2) + 18;

        enableCommandsButton = new GUICheckBox(200, calc1, ExodusClient.GUIS.getButtonY(3), Constants.TRANSLATOR.translate("gui.config.name.general.enablecommands"), ExodusClient.CONFIG.enableCommands);
        enableFullBrightButton = new GUICheckBox(300, calc2, ExodusClient.GUIS.getButtonY(3), Constants.TRANSLATOR.translate("gui.config.name.general.fullbright"), ExodusClient.CONFIG.fullBright);

        proceedButton = new GUIExtendedButton(1100, (sr.getScaledWidth() / 2) - 90, (sr.getScaledHeight() - 30), 180, 20, Constants.TRANSLATOR.translate("gui.config.buttonMessage.back"));

        buttonList.add(enableCommandsButton);
        buttonList.add(enableFullBrightButton);

        buttonList.add(proceedButton);

        super.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        ScaledResolution sr = new ScaledResolution(mc);
        drawDefaultBackground();

        final String mainTitle = Constants.TRANSLATOR.translate("gui.config.title");
        final String title = Constants.TRANSLATOR.translate("gui.config.title.general");

        drawString(fontRenderer, mainTitle, (sr.getScaledWidth() / 2) - (fontRenderer.getStringWidth(mainTitle) / 2), 10, 0xFFFFFF);
        drawString(fontRenderer, title, (sr.getScaledWidth() / 2) - (fontRenderer.getStringWidth(title) / 2), 20, 0xFFFFFF);

        super.drawScreen(mouseX, mouseY, partialTicks);

        if (ExodusClient.GUIS.isMouseOver(mouseX, mouseY, enableCommandsButton)) {
            ExodusClient.GUIS.drawMultiLineString(StringHandler.splitTextByNewLine(Constants.TRANSLATOR.translate("gui.config.comment.general.enablecommands")), mouseX, mouseY, width, height, -1, fontRenderer, true);
        }
        if (ExodusClient.GUIS.isMouseOver(mouseX, mouseY, enableFullBrightButton)) {
            ExodusClient.GUIS.drawMultiLineString(StringHandler.splitTextByNewLine(Constants.TRANSLATOR.translate("gui.config.comment.general.fullbright")), mouseX, mouseY, width, height, -1, fontRenderer, true);
        }
        if (ExodusClient.GUIS.isMouseOver(mouseX, mouseY, proceedButton) && !proceedButton.enabled) {
            ExodusClient.GUIS.drawMultiLineString(StringHandler.splitTextByNewLine(Constants.TRANSLATOR.translate("gui.config.hoverMessage.defaultempty")), mouseX, mouseY, width, height, -1, fontRenderer, true);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == proceedButton.id) {
            if (enableCommandsButton.isChecked() != ExodusClient.CONFIG.enableCommands) {
                ExodusClient.CONFIG.hasChanged = true;
                ExodusClient.CONFIG.enableCommands = enableCommandsButton.isChecked();
            }
            if (enableFullBrightButton.isChecked() != ExodusClient.CONFIG.fullBright) {
                ExodusClient.CONFIG.hasChanged = true;
                ExodusClient.CONFIG.fullBright = enableFullBrightButton.isChecked();
            }
            mc.displayGuiScreen(parentScreen);
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            mc.displayGuiScreen(parentScreen);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }
}
