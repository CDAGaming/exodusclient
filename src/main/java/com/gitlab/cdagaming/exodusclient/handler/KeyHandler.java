package com.gitlab.cdagaming.exodusclient.handler;

import com.gitlab.cdagaming.exodusclient.Constants;
import com.gitlab.cdagaming.exodusclient.ExodusClient;
import com.google.common.collect.Lists;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;

import java.util.Arrays;
import java.util.List;

public class KeyHandler {
    private List<KeyBinding> keyBindings = Lists.newArrayList();
    private KeyBinding configKeybinding, fullBrightKeybinding;

    public void register() {
        keyBindings.addAll(Arrays.asList(ExodusClient.instance.gameSettings.keyBindings));

        // Module KeyBindings
        fullBrightKeybinding = new KeyBinding(Constants.TRANSLATOR.translate("key.exodusclient.fullbright_keybind"), Keyboard.CHAR_NONE, Constants.TRANSLATOR.translate("key.exodusclient.category"));
        configKeybinding = new KeyBinding(Constants.TRANSLATOR.translate("key.exodusclient.config_keybind"), Keyboard.KEY_RCONTROL, Constants.TRANSLATOR.translate("key.exodusclient.category"));

        keyBindings.add(fullBrightKeybinding);
        keyBindings.add(configKeybinding);

        ExodusClient.instance.gameSettings.keyBindings = keyBindings.toArray(new KeyBinding[]{});
    }

    public void onTick() {
        if (configKeybinding != null) {
            if (configKeybinding.getKeyCode() < 0) {
                Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.keybind"));
                configKeybinding.setKeyCode(Keyboard.KEY_RCONTROL);
            } else if (Keyboard.isKeyDown(configKeybinding.getKeyCode()) && !(ExodusClient.instance.currentScreen instanceof GuiControls) && !ExodusClient.GUIS.openConfigGUI && !ExodusClient.GUIS.configGUIOpened) {
                ExodusClient.GUIS.openConfigGUI = true;
            }
        }
    }
}
