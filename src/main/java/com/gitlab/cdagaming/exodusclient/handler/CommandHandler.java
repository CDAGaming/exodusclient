package com.gitlab.cdagaming.exodusclient.handler;

import com.gitlab.cdagaming.exodusclient.Constants;
import com.gitlab.cdagaming.exodusclient.ExodusClient;
import net.minecraft.client.Minecraft;

public class CommandHandler {
    public static void reloadData() {
        ExodusClient.instance = Minecraft.getMinecraft();
        ExodusClient.player = ExodusClient.instance.player;

        Constants.TRANSLATOR.tick();
        ExodusClient.KEYBINDINGS.onTick();
        ExodusClient.GUIS.onTick();
        ExodusClient.PLAYER.onTick();
    }

    public static void init() {
        // Initialization Events
    }
}
