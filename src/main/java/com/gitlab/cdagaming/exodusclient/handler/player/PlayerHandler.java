package com.gitlab.cdagaming.exodusclient.handler.player;

import com.gitlab.cdagaming.exodusclient.ExodusClient;

public class PlayerHandler {
    public void onTick() {
        if (ExodusClient.player != null) {
            // FULLBRIGHT MODULE
            if (ExodusClient.CONFIG.fullBright && ExodusClient.instance.gameSettings.gammaSetting != 16f) {
                ExodusClient.instance.gameSettings.gammaSetting = 16f;
            }
        }
    }
}
