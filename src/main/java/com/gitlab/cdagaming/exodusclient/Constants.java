package com.gitlab.cdagaming.exodusclient;

import com.gitlab.cdagaming.exodusclient.handler.TranslationHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.realms.RealmsSharedConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Constants {
    public static final String NAME = "GRADLE:mod_name";
    public static final String majorVersion = "GRADLE:majorVersion";
    public static final String minorVersion = "GRADLE:minorVersion";
    public static final String revisionVersion = "GRADLE:revisionVersion";
    public static final String VERSION_ID = "v" + majorVersion + "." + minorVersion + "." + revisionVersion;
    public static final String MODID = "exodusclient";
    public static final String GUI_FACTORY = "com.gitlab.cdagaming.exodusclient.config.ConfigGUIFactoryDS";
    public static final String MCVersion = RealmsSharedConstants.VERSION_STRING;
    public static final String configDir = Minecraft.getMinecraft().gameDir + File.separator + "config";
    public static final String modsDir = Minecraft.getMinecraft().gameDir + File.separator + "mods";
    public static final String UPDATE_JSON = "https://gitlab.com/CDAGaming/VersionLibrary/raw/master/ExodusClient/update.json";
    public static final Logger LOG = LogManager.getLogger(MODID);
    public static final TranslationHandler TRANSLATOR = new TranslationHandler(MODID, false);
    public static final ClassLoader CLASS_LOADER = Thread.currentThread().getContextClassLoader();
}
