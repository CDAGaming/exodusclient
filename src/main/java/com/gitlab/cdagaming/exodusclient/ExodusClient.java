package com.gitlab.cdagaming.exodusclient;

import com.gitlab.cdagaming.exodusclient.config.ConfigHandler;
import com.gitlab.cdagaming.exodusclient.handler.CommandHandler;
import com.gitlab.cdagaming.exodusclient.handler.KeyHandler;
import com.gitlab.cdagaming.exodusclient.handler.SystemHandler;
import com.gitlab.cdagaming.exodusclient.handler.gui.GUIHandler;
import com.gitlab.cdagaming.exodusclient.handler.player.PlayerHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLModDisabledEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.io.File;

@Mod(modid = Constants.MODID, name = Constants.NAME, version = Constants.VERSION_ID, clientSideOnly = true, guiFactory = Constants.GUI_FACTORY, updateJSON = Constants.UPDATE_JSON, acceptedMinecraftVersions = "*")
public class ExodusClient {
    public static Minecraft instance = Minecraft.getMinecraft();
    public static EntityPlayer player = instance.player;
    public static int TIMER = 0;

    public static ConfigHandler CONFIG;
    public static SystemHandler SYSTEM;
    public static KeyHandler KEYBINDINGS = new KeyHandler();
    public static GUIHandler GUIS = new GUIHandler();
    public static PlayerHandler PLAYER = new PlayerHandler();

    private static Thread timerThread = new Thread("ExodusClient-Timer") {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                if (TIMER > 0) {
                    TIMER--;
                    try {
                        Thread.sleep(1000L);
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    };

    private boolean initialized = false;

    public ExodusClient() {
        MinecraftForge.EVENT_BUS.register(this);
        KEYBINDINGS.register();
        timerThread.start();
    }

    @Mod.EventHandler
    public void disableMod(final FMLModDisabledEvent event) {
        // ShutDown Events
    }

    public void init() {
        SYSTEM = new SystemHandler();
        CONFIG = new ConfigHandler(Constants.configDir + File.separator + Constants.MODID + ".properties");
        CONFIG.initialize();

        CommandHandler.init();

        try {
            // Initialization Events
        } catch (Exception ex) {
            Constants.LOG.error(Constants.TRANSLATOR.translate("exodusclient.logger.error.load"));
            ex.printStackTrace();
        } finally {
            initialized = true;
        }
    }

    @SubscribeEvent
    public void onTick(final TickEvent.ClientTickEvent event) {
        if (!initialized && instance.currentScreen != null && (
                instance.currentScreen instanceof GuiMainMenu ||
                        instance.currentScreen.getClass().getSimpleName().equals("GuiCustom") ||
                        instance.currentScreen.getClass().getSimpleName().contains("MainMenu") || (
                        instance.currentScreen.getClass().getSuperclass() != null &&
                                instance.currentScreen.getClass().getSuperclass().getSimpleName().equals(GuiMainMenu.class.getSimpleName())))) {
            init();
        } else if (initialized) {
            CommandHandler.reloadData();

            if (!CONFIG.hasChanged) {
                //
            }
        }
    }
}
